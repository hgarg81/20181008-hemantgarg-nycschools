Hi
This project is created by Hemant Garg for interview at JP Morgan Chase.

I have added apk files under APK folder so that interview panel can quickly run 
the app on their devices.

Architecture -
I implemented MVVM design pattern using LiveData, ViewModel and repository.
I used some 3rd party library to appmpolish the task.
It's difficult to make Map work properly with orientation changes.



User Interface - 
The idea is to show all the schools on the map with or witout user's location.
Then user can also search for special keyboards supported as full text search on server side.
User will see those results as pin on the map.

Clicking on the cluster will show the details along with requested SAT scores.

Unit Testing - 
I started unit testing from data layer to ViewModel.
Did basic ones to show how to proceed in that direction.
I am familiar with small, medium and large testing patterns using JUNIT, Mockito, Roboelectric and Expresso.

If given more time - 
- ofcourse this is still not production ready
- I have to Crashlytics integration, GA integration
- I have to fix styles and theme properly.
- I want to test the app on older OS version
- I wanted to make dynamic search filters like LinkedIn has for eg "Location", "Job" etc.
  I started going on that path and implemented a cache to do read all possible filters etc 
  But for the sake of time dropped that idea. That costed me not able to start on unit testing.
- And add more and more and more of Unit Testing and automation testing.
