package com.example.hemant.a20181003_hemantgarg_nycschools.repo;

import android.arch.core.executor.testing.InstantTaskExecutorRule;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.hemant.a20181003_hemantgarg_nycschools.R;
import com.example.hemant.a20181003_hemantgarg_nycschools.mockwebservice.MockInternetErrorSchoolWebService;
import com.example.hemant.a20181003_hemantgarg_nycschools.mockwebservice.MockSchoolWebService;
import com.example.hemant.a20181003_hemantgarg_nycschools.mockwebservice.MockServerErrorSchoolWebService;
import com.example.hemant.a20181003_hemantgarg_nycschools.models.SchoolFilter;
import com.example.hemant.a20181003_hemantgarg_nycschools.models.SchoolsResponse;
import com.example.hemant.a20181003_hemantgarg_nycschools.network.SchoolWebService;
import com.example.hemant.a20181003_hemantgarg_nycschools.utils.ErrorUtils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

/**
 * Test the repo with fake webservice
 */
public class SchoolRepoTest {

    @Mock
    Context mMockContext;

    @Mock
    ConnectivityManager mConnectivityManager;

    @Mock
    NetworkInfo mNetworkInfo;

//
    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    private MockRetrofit mMockRetrofit;

    private ExecutorService bg = Executors.newCachedThreadPool();

    private static final int WAITING_TIME = 10;//25;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://data.cityofnewyork.us/")
                .build();


        NetworkBehavior behavior = NetworkBehavior.create();
        behavior.setDelay(2, TimeUnit.SECONDS);

        bg = Executors.newSingleThreadExecutor();

        mMockRetrofit = new MockRetrofit.Builder(retrofit)
                .networkBehavior(behavior)
                .backgroundExecutor(bg)
                .build();

        Mockito.when(mMockContext.getSystemService(Context.CONNECTIVITY_SERVICE))
                .thenReturn(mConnectivityManager);

        Mockito.when(mConnectivityManager.getActiveNetworkInfo())
                .thenReturn(mNetworkInfo);

        Mockito.when(mMockContext.getString(R.string.error_network_server_down))
                .thenReturn("Sometimes we take a break too!!");
    }

    @Test
    public void getSchools_search() throws InterruptedException {

        MockSchoolWebService mockService =
                new MockSchoolWebService(mMockRetrofit.create(SchoolWebService.class));

        Mockito.when(mNetworkInfo.isConnected())
                .thenReturn(true);

        SchoolRepository repo = SchoolRepositoryImp.getInstance(mMockContext, mockService);

        SchoolFilter schoolFilter = new SchoolFilter();
        schoolFilter.searchTerm = "foo";

        MutableLiveData<SchoolsResponse> liveData = new MutableLiveData<>();

        repo.getSchoolList(liveData, schoolFilter);

        liveData.observeForever(schoolsResponse -> {
            Assert.assertEquals(1, schoolsResponse.schools.size());
            Assert.assertEquals("foo", schoolsResponse.schools.get(0).id);
        });

        bg.awaitTermination(WAITING_TIME, TimeUnit.SECONDS);
        Assert.assertEquals(1, liveData.getValue().schools.size());
    }

    @Test
    public void getSchools_serviceConnectionError() throws InterruptedException {
        MockServerErrorSchoolWebService mockService =
                new MockServerErrorSchoolWebService(mMockRetrofit.create(SchoolWebService.class));

        Mockito.when(mNetworkInfo.isConnected())
                .thenReturn(true);

        SchoolRepository repo = SchoolRepositoryImp.getInstance(mMockContext, mockService);

        SchoolFilter schoolFilter = new SchoolFilter();

        MutableLiveData<SchoolsResponse> liveData = new MutableLiveData<>();

        repo.getSchoolList(liveData, schoolFilter);

        liveData.observeForever(schoolsResponse -> {
            Assert.assertEquals(0, schoolsResponse.schools.size());
            Assert.assertEquals(ErrorUtils.NETWORK_ERROR_SERVER_DOWN,
                    schoolsResponse.status.code);
        });

        bg.awaitTermination(WAITING_TIME, TimeUnit.SECONDS);
        Assert.assertEquals(ErrorUtils.NETWORK_ERROR_SERVER_DOWN,
                liveData.getValue().status.code);
    }

    @Test
    public void getSchools_whenNoInternetAvailable() throws InterruptedException {
        MockInternetErrorSchoolWebService mockService =
                new MockInternetErrorSchoolWebService(mMockRetrofit.create(SchoolWebService.class));

        Mockito.when(mNetworkInfo.isConnected())
                .thenReturn(false);

        SchoolRepository repo = SchoolRepositoryImp.getInstance(mMockContext, mockService);

        SchoolFilter schoolFilter = new SchoolFilter();

        MutableLiveData<SchoolsResponse> liveData = new MutableLiveData<>();

        repo.getSchoolList(liveData, schoolFilter);

        liveData.observeForever(schoolsResponse -> {
            Assert.assertEquals(0, schoolsResponse.schools.size());
            Assert.assertEquals(ErrorUtils.NETWORK_ERROR_DEVICE_HAS_NO_NETWORK,
                    schoolsResponse.status.code);
        });

        bg.awaitTermination(WAITING_TIME, TimeUnit.SECONDS);
        Assert.assertEquals(ErrorUtils.NETWORK_ERROR_DEVICE_HAS_NO_NETWORK,
                liveData.getValue().status.code);
    }

    @Test
    public void getSchools_whenServerReturnsError() throws InterruptedException {
        MockServerErrorSchoolWebService mockService =
                new MockServerErrorSchoolWebService(mMockRetrofit.create(SchoolWebService.class));

        Mockito.when(mNetworkInfo.isConnected())
                .thenReturn(true);

        SchoolRepository repo = SchoolRepositoryImp.getInstance(mMockContext, mockService);

        SchoolFilter schoolFilter = new SchoolFilter();

        MutableLiveData<SchoolsResponse> liveData = new MutableLiveData<>();

        repo.getSchoolList(liveData, schoolFilter);

        liveData.observeForever(schoolsResponse -> {
            Assert.assertEquals(0, schoolsResponse.schools.size());
            Assert.assertEquals("400",
                    schoolsResponse.status.code);
        });

        bg.awaitTermination(WAITING_TIME, TimeUnit.SECONDS);
        Assert.assertEquals("400",
                liveData.getValue().status.code);
    }
}
