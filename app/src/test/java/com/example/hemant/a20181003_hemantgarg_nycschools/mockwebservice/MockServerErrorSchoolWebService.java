package com.example.hemant.a20181003_hemantgarg_nycschools.mockwebservice;

import android.util.Log;

import com.example.hemant.a20181003_hemantgarg_nycschools.models.SATScore;
import com.example.hemant.a20181003_hemantgarg_nycschools.models.School;
import com.example.hemant.a20181003_hemantgarg_nycschools.models.Status;
import com.example.hemant.a20181003_hemantgarg_nycschools.network.SchoolWebService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.Calls;

public class MockServerErrorSchoolWebService implements SchoolWebService {

    private final BehaviorDelegate<SchoolWebService> delegate;

    public MockServerErrorSchoolWebService(BehaviorDelegate<SchoolWebService> service) {
        this.delegate = service;
    }

    @Override
    public Call<List<School>> getSchool(String searchText) {
        Status status = new Status();
        status.code = "400";
        status.message = "No schools found";


        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = "";
        try {
            json = ow.writeValueAsString(status);
            Response response = Response.error(404, ResponseBody.create(MediaType.parse("application/json") ,json));
            return delegate.returning(Calls.response(response)).getAllSchools();
        } catch (JsonProcessingException e) {
            return Calls.failure(e);
        }
    }

    @Override
    public Call<List<School>> getAllSchools() {
        Status status = new Status();
        status.code = "400";
        status.message = "No schools found";


        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = "";
        try {
            json = ow.writeValueAsString(status);
            Response response = Response.error(404, ResponseBody.create(MediaType.parse("application/json") ,json));
            return delegate.returning(Calls.response(response)).getAllSchools();
        } catch (JsonProcessingException e) {
            return Calls.failure(e);
        }
    }

    @Override
    public Call<List<SATScore>> getSATScore(String schoolId) {
        return null;
    }
}
