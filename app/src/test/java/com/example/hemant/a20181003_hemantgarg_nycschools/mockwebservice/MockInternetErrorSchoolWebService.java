package com.example.hemant.a20181003_hemantgarg_nycschools.mockwebservice;

import com.example.hemant.a20181003_hemantgarg_nycschools.models.SATScore;
import com.example.hemant.a20181003_hemantgarg_nycschools.models.School;
import com.example.hemant.a20181003_hemantgarg_nycschools.models.Status;
import com.example.hemant.a20181003_hemantgarg_nycschools.network.SchoolWebService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.Calls;

public class MockInternetErrorSchoolWebService implements SchoolWebService {

    private final BehaviorDelegate<SchoolWebService> delegate;

    public MockInternetErrorSchoolWebService(BehaviorDelegate<SchoolWebService> service) {
        this.delegate = service;
    }

    @Override
    public Call<List<School>> getSchool(String searchText) {
        IOException ex = new IOException();
        return Calls.failure(ex);
    }

    @Override
    public Call<List<School>> getAllSchools() {
        IOException ex = new IOException();
        return Calls.failure(ex);
    }

    @Override
    public Call<List<SATScore>> getSATScore(String schoolId) {
        IOException ex = new IOException();
        return Calls.failure(ex);
    }
}
