package com.example.hemant.a20181003_hemantgarg_nycschools.mockwebservice;

import android.content.Context;

import com.example.hemant.a20181003_hemantgarg_nycschools.models.School;
import com.example.hemant.a20181003_hemantgarg_nycschools.models.Status;
import com.example.hemant.a20181003_hemantgarg_nycschools.network.SchoolWebService;
import com.example.hemant.a20181003_hemantgarg_nycschools.utils.ErrorUtils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

public class WebServerTest {

    private MockRetrofit mMockRetrofit;

    @Mock
    Context mMockContext;

    @Before
    public void setUp() throws Exception {

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://data.cityofnewyork.us/")
                .build();


        NetworkBehavior behavior = NetworkBehavior.create();
        mMockRetrofit = new MockRetrofit.Builder(retrofit)
                .networkBehavior(behavior)
                .build();
    }

    @Test
    public void testParsing() throws Exception {
        MockSchoolWebService mockService =
                new MockSchoolWebService(mMockRetrofit.create(SchoolWebService.class));

        //Actual Test
        Response<List<School>> response = mockService.getAllSchools().execute();

        Assert.assertTrue(response.isSuccessful());

        List<School> schools = new ArrayList<>(response.body());

        //Asserting response
        Assert.assertEquals(schools.size(), 2);
        Assert.assertEquals(schools.get(0).id, "TestId1");
        Assert.assertEquals(schools.get(0).name, "TestName1");
        Assert.assertEquals(schools.get(1).id, "TestId2");
        Assert.assertEquals(schools.get(1).name, "TestName2");

    }

    @Test
    public void testServerError() throws Exception {
        MockServerErrorSchoolWebService mockService =
                new MockServerErrorSchoolWebService(mMockRetrofit.create(SchoolWebService.class));

        //Actual Test
        Response<List<School>> response = mockService.getAllSchools().execute();

        Assert.assertFalse(response.isSuccessful());

        Status status = ErrorUtils.handleWebserviceError(mMockContext, response);

        //Asserting response
        Assert.assertEquals("400", status.code);
        Assert.assertEquals("No schools found", status.message);

    }

    @Test
    public void testNetworkError() throws Exception {
        MockServerErrorSchoolWebService mockService
                = new MockServerErrorSchoolWebService(mMockRetrofit.create(SchoolWebService.class));

        try{
            mockService.getAllSchools().execute();
        }catch (IOException ex){
            Status status = ErrorUtils.handleWebserviceError(mMockContext, ex);

            //Asserting response
            Assert.assertEquals(ErrorUtils.NETWORK_ERROR_SERVER_DOWN, status.code);
        }
    }


}
