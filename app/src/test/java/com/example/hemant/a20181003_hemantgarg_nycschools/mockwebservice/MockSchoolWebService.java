package com.example.hemant.a20181003_hemantgarg_nycschools.mockwebservice;

import com.example.hemant.a20181003_hemantgarg_nycschools.models.SATScore;
import com.example.hemant.a20181003_hemantgarg_nycschools.models.School;
import com.example.hemant.a20181003_hemantgarg_nycschools.network.SchoolWebService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.mock.BehaviorDelegate;

public class MockSchoolWebService implements SchoolWebService {

    private final BehaviorDelegate<SchoolWebService> delegate;

    public MockSchoolWebService(BehaviorDelegate<SchoolWebService> service) {
        this.delegate = service;
    }

    @Override
    public Call<List<School>> getSchool(String searchText) {
        List<School> result = new ArrayList<>();

        School school = new School();
        school.id = searchText;
        school.name = searchText+"Name";

        result.add(school);

        return delegate.returningResponse(result).getAllSchools();
    }

    @Override
    public Call<List<School>> getAllSchools() {
        List<School> result = new ArrayList<>();

        School school = new School();
        school.id = "TestId1";
        school.name = "TestName1";

        School school2 = new School();
        school2.id = "TestId2";
        school2.name = "TestName2";

        result.add(school);
        result.add(school2);

        return delegate.returningResponse(result).getAllSchools();
    }

    @Override
    public Call<List<SATScore>> getSATScore(String schoolId) {
        return null;
    }
}
