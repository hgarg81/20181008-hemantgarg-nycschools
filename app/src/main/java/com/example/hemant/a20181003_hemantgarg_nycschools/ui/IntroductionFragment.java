package com.example.hemant.a20181003_hemantgarg_nycschools.ui;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hemant.a20181003_hemantgarg_nycschools.R;

/**
 * To show about what is this app about.
 * On purpose, I want to show them this on every app launch.
 */
public class IntroductionFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_introduction, container, false);

        view.findViewById(R.id.next_btn).setOnClickListener(v -> {
            FragmentManager fm = getActivity().getSupportFragmentManager();

            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.fragment_layout, new SchoolMapFragment());
            ft.remove(IntroductionFragment.this);
            ft.commit();
        });
        return view;
    }

}
