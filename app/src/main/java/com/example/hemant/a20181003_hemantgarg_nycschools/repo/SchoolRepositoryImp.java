package com.example.hemant.a20181003_hemantgarg_nycschools.repo;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.text.TextUtils;

import com.example.hemant.a20181003_hemantgarg_nycschools.models.SATScore;
import com.example.hemant.a20181003_hemantgarg_nycschools.models.School;
import com.example.hemant.a20181003_hemantgarg_nycschools.models.SchoolFilter;
import com.example.hemant.a20181003_hemantgarg_nycschools.models.SchoolWithSATResponse;
import com.example.hemant.a20181003_hemantgarg_nycschools.models.SchoolsResponse;
import com.example.hemant.a20181003_hemantgarg_nycschools.network.SchoolWebService;
import com.example.hemant.a20181003_hemantgarg_nycschools.utils.ErrorUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * This is connecting link between application and the data source.
 * The rest of the application shouldn't work about how the data is coming.
 * It can come via SQL, in-memory cache or network call.
 * At the same time, Repository shouldn't worry about maintaining various LiveData.
 * It is LiveData aware and wants to send the data back via LiveData.
 *
 * I could have made a database to cache the data and hit network only when required.
 * But I thought it was overkill for so many columns and only 440 schools.
 * At the end, I ended up only reading few data fields and cache them in a map for faster access.
 *
 */
public class SchoolRepositoryImp implements SchoolRepository{

    private SchoolWebService mWebService;
    private Context mContext;

    private static SchoolRepository sSchoolRepositoryImp;

    private Map<String, School> mSchoolMap;
    private Map<String, SATScore> mSatScoreMap;

    public synchronized static SchoolRepository getInstance(Context context,
                                                            SchoolWebService webService){
        if (sSchoolRepositoryImp == null){
            sSchoolRepositoryImp = new SchoolRepositoryImp(context, webService);
        }
        return sSchoolRepositoryImp;
    }

    private SchoolRepositoryImp(Context context,
                                SchoolWebService webService){
        mWebService = webService;
        mContext = context;
        mSchoolMap = new HashMap<>();
        mSatScoreMap = new HashMap<>();

    }

    @Override
    public void getSchoolWithSATById(final MutableLiveData<SchoolWithSATResponse> liveData,
                                                                final String id){

        if(liveData == null){
            throw new NullPointerException("Please send non empty LiveData to me");
        }

        // Though caching is not required and we can fetch both school and SAT scores again
        if (mSchoolMap.containsKey(id)){

            if(!mSatScoreMap.containsKey(id)) {
                getSATScoreFromNetwork(liveData, id);
            }

            // set the data inside LiveData
            SchoolWithSATResponse schoolRes = new SchoolWithSATResponse();
            schoolRes.school = mSchoolMap.get(id);
            schoolRes.satScore = mSatScoreMap.get(id);
            liveData.setValue(schoolRes);
        }
    }

    /**
     * I initially planned to make LinkedIn kind of complex filter/search.
     * @param filter
     * @return
     */
    @Override
    public void getSchoolList(final MutableLiveData<SchoolsResponse> liveData,
                                                          final SchoolFilter filter){

        if(liveData == null){
            throw new NullPointerException("Please send non empty LiveData to me");
        }

        // Repo is not supposed to delete the LiveData before putting new data.
        // UI may be relying on old data for some reason..

        getSchoolListFromNetwork(liveData, filter);
    }

    private void getSchoolListFromNetwork(final MutableLiveData<SchoolsResponse> liveData,
                                          final SchoolFilter initialFilter){
        Call<List<School>> call = null;

        if (initialFilter != null && !TextUtils.isEmpty(initialFilter.searchTerm)){
            call = mWebService.getSchool(initialFilter.searchTerm);
        } else{
            call = mWebService.getAllSchools();
        }

        call.enqueue(new Callback<List<School>>() {
            @Override
            public void onResponse(Call<List<School>> call, Response<List<School>> response) {
                SchoolsResponse schoolRes = new SchoolsResponse();

                if (response.isSuccessful() && response.body() != null){
                    mSchoolMap = response.body().stream()
                            .collect(Collectors.toMap(School::getId, school -> school));

                    schoolRes.schools.addAll(mSchoolMap.values());

                } else {
                    schoolRes.status = ErrorUtils.handleWebserviceError(mContext, response);
                }
                liveData.setValue(schoolRes);
            }

            @Override
            public void onFailure(Call<List<School>> call, Throwable t) {
                SchoolsResponse schoolRes = new SchoolsResponse();
                schoolRes.status = ErrorUtils.handleWebserviceError(mContext, t);
                liveData.setValue(schoolRes);
            }
        });
    }

    private void getSATScoreFromNetwork(final MutableLiveData<SchoolWithSATResponse> liveData, final String id){
        mWebService.getSATScore(id).enqueue(new Callback<List<SATScore>>() {
            @Override
            public void onResponse(Call<List<SATScore>> call, Response<List<SATScore>> response) {
                SchoolWithSATResponse schoolRes = new SchoolWithSATResponse();
                schoolRes.school = mSchoolMap.get(id);

                if (response.isSuccessful()
                        && response.body() != null
                        && !response.body().isEmpty()){
                    SATScore score = response.body().get(0);

                    mSatScoreMap.put(id, score);
                    schoolRes.satScore = score;
                } else {
                    schoolRes.staScoreStatus = ErrorUtils.handleWebserviceError(mContext, response);
                }
                liveData.setValue(schoolRes);
            }

            @Override
            public void onFailure(Call<List<SATScore>> call, Throwable t) {
                SchoolWithSATResponse schoolRes = new SchoolWithSATResponse();
                schoolRes.school = mSchoolMap.get(id);

                schoolRes.staScoreStatus = ErrorUtils.handleWebserviceError(mContext, t);
                liveData.setValue(schoolRes);
            }
        });
    }

}
