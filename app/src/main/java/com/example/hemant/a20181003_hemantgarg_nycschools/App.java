package com.example.hemant.a20181003_hemantgarg_nycschools;

import android.app.Application;
import android.content.Context;

public class App extends Application {
    private static Context sContext;

    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
    }

    public static Context getAppContext() {
        return sContext;
    }
}
