package com.example.hemant.a20181003_hemantgarg_nycschools.ui.jetpack;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.example.hemant.a20181003_hemantgarg_nycschools.repo.SchoolRepository;

public class ViewModelFactory implements ViewModelProvider.Factory {

    private final SchoolRepository mSchoolRepository;

    public ViewModelFactory(SchoolRepository schoolRepository){
        mSchoolRepository = schoolRepository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(SchoolListViewModel.class)) {
            return (T) new SchoolListViewModel(mSchoolRepository);
        } else if (modelClass.isAssignableFrom(SchoolDetailViewModel.class)) {
            return (T) new SchoolDetailViewModel(mSchoolRepository);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
