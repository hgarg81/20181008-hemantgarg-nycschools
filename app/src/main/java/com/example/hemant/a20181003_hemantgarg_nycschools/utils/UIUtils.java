package com.example.hemant.a20181003_hemantgarg_nycschools.utils;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class UIUtils {

    public static void hideKeyboard(Activity context, View focusedView){
        InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
    }

    public static String getPercentage(String value){
        String percent = "-";

        try{
            if(!TextUtils.isEmpty(value)) {
                percent = String.valueOf(Math.round(Double.parseDouble(value) * 100)) + "%";
            }
        }catch (NumberFormatException ex){
            //TODO I would later log this to Crashlytics to track which data is bad on server
        }
        return percent;
    }

}
