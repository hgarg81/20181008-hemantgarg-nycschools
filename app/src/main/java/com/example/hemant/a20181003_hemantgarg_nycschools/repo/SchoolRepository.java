package com.example.hemant.a20181003_hemantgarg_nycschools.repo;

import android.arch.lifecycle.MutableLiveData;

import com.example.hemant.a20181003_hemantgarg_nycschools.models.SchoolFilter;
import com.example.hemant.a20181003_hemantgarg_nycschools.models.SchoolWithSATResponse;
import com.example.hemant.a20181003_hemantgarg_nycschools.models.SchoolsResponse;

public interface SchoolRepository {

    public void getSchoolWithSATById(final MutableLiveData<SchoolWithSATResponse> liveData,
                                     final String id);

    public void getSchoolList(final MutableLiveData<SchoolsResponse> liveData,
                              final SchoolFilter filter);


}
