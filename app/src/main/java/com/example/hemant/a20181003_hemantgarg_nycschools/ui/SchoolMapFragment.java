package com.example.hemant.a20181003_hemantgarg_nycschools.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.hemant.a20181003_hemantgarg_nycschools.R;
import com.example.hemant.a20181003_hemantgarg_nycschools.models.SchoolFilter;
import com.example.hemant.a20181003_hemantgarg_nycschools.repo.SchoolRepositoryImp;
import com.example.hemant.a20181003_hemantgarg_nycschools.ui.jetpack.Injection;
import com.example.hemant.a20181003_hemantgarg_nycschools.ui.jetpack.LocationViewModel;
import com.example.hemant.a20181003_hemantgarg_nycschools.ui.jetpack.SchoolListViewModel;
import com.example.hemant.a20181003_hemantgarg_nycschools.ui.map.LocationConstants;
import com.example.hemant.a20181003_hemantgarg_nycschools.ui.map.SchoolClusterPresenter;
import com.example.hemant.a20181003_hemantgarg_nycschools.utils.AlertPresenters;
import com.example.hemant.a20181003_hemantgarg_nycschools.utils.LocationPermissionPresenter;
import com.example.hemant.a20181003_hemantgarg_nycschools.utils.UIUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;

/**
 * This binds map and search and location.
 * I am using LiveData to fetch data and manipulate the UI.
 */

public class SchoolMapFragment extends Fragment implements OnMapReadyCallback {


    private static final String SI_SHALL_MOVE_CAMERA_BASED_ON_LOCATION = "SI_SHALL_MOVE_CAMERA_BASED_ON_LOCATION";

    //Todo - Use butterknife and dependency injection

    // ---------- UI Stuff ----------
    private ViewGroup mEmptyResultView;
    private EditText mEditText;
    private ProgressBar mProgressBar;

    // ----- ViewModels which powers the UI -----------
    private SchoolListViewModel mViewModel;
    private LocationViewModel mLocationViewModel;

    // -------- To Manage Map Clusters ----
    private SchoolClusterPresenter mSchoolClusterPresenter;
    private MapView mMapView;
    private GoogleMap mGmap;

    private boolean shallMoveCameraToLocation;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_school_map, container, false);


        if(savedInstanceState != null){
            shallMoveCameraToLocation = savedInstanceState.getBoolean(SI_SHALL_MOVE_CAMERA_BASED_ON_LOCATION);
        }

        // UI referencing
        //TODO - Buterknife
        mEmptyResultView = view.findViewById(R.id.empty_result_layout);
        mEditText = view.findViewById(R.id.search_et);
        mProgressBar = view.findViewById(R.id.progress_bar);


        // setup Map stuff; we have to display the map fastest
        mMapView = view.findViewById(R.id.map_view);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);


        // Get ViewModels ready to start getting data
        // They don't fetch the data themselve; We have to trigger that.
        setupLocationViewModel();

        setupSchoolViewModel();

        // Set usual click listeners
        setClickListeners(view);

        if (savedInstanceState == null) {
            triggerSchoolDataFetch();
        }

        return view;
    }


    // --------- Location stuff - ViewModel and Permissions -----------------------



    private void setupLocationViewModel() {
        // null checks protects that someone later should not call it twice.
        if (mLocationViewModel == null) {
            mLocationViewModel = ViewModelProviders
                    .of(this.getActivity()).get(LocationViewModel.class);

            mLocationViewModel.getLiveData().observe(this, location -> {
                if(location == null){
                    LocationPermissionPresenter.requestLocationPermission(this.getActivity());
                } else if(shallMoveCameraToLocation){
                    // if user has not interacted with Map yet then focus on user's location
                    moveMapCamera(null, 0);
                }
            });
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == LocationPermissionPresenter.MY_PERMISSIONS_REQUEST_LOCATION
                && grantResults[0] == Activity.RESULT_OK){

            mLocationViewModel.triggerLocationUpdate();
            if(mGmap != null){
                mGmap.setMyLocationEnabled(true);
            }

        }
    }

    // --------- SchoolList stuff - ViewModel and triggering search  -----------------------
    private void setupSchoolViewModel(){
        if(mViewModel == null) {

            mViewModel = ViewModelProviders
                    .of(this.getActivity(), Injection.provideViewModelFactory(this.getActivity()))
                    .get(SchoolListViewModel.class);

            mViewModel.getLiveData().observe(this, schoolsResponse -> {

                mEmptyResultView.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.GONE);

                if (schoolsResponse.status != null) {

                    // show error
                    AlertPresenters.showErrorAlert(getActivity(), schoolsResponse.status.message);

                } else if (schoolsResponse.schools.isEmpty()) {
                    // show empty
                    mEmptyResultView.setVisibility(View.VISIBLE);
                }

                // always refresh the pins so that we can clear on bad result
                loadPinsOnMap();
            });
        }
    }

    private void triggerSchoolDataFetch(){
        mProgressBar.setVisibility(View.VISIBLE);
        UIUtils.hideKeyboard(getActivity(), mEditText);
        SchoolFilter schoolFilter = new SchoolFilter();
        schoolFilter.searchTerm = mEditText.getText().toString().trim();
        mViewModel.getFilteredSchoolList(schoolFilter);
    }

    private void setClickListeners(View view){

        view.findViewById(R.id.show_all_btn).setOnClickListener(v -> {
            shallMoveCameraToLocation = false;

            mEditText.setText("");
            triggerSchoolDataFetch();
        });


        view.findViewById(R.id.location_btn).setOnClickListener(v -> {
            shallMoveCameraToLocation = true;

            // get current location
            mLocationViewModel.triggerLocationUpdate();

        });

        mEditText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                triggerSchoolDataFetch();
                return true;
            }
            return false;
        });

        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                mEmptyResultView.setVisibility(View.GONE);
            }
        });
    }


    // --------- Map stuff - ViewModel and loading pins etc  -----------------------

    // I saw weird bug that Google map took extra time
    private boolean didWeMissPinLoading = false;

    private void loadPinsOnMap(){

        // there is a slight chance that Map may not be ready
        if(mGmap == null || mSchoolClusterPresenter == null){
            didWeMissPinLoading = true;
            return;
        }


        boolean foundAtleastOneLocation = mSchoolClusterPresenter.loadClusters(mViewModel.getLiveData().getValue().schools);
        if(foundAtleastOneLocation){
            shallMoveCameraToLocation = false;
        }

    }

    private void moveMapCamera(LatLng latLng, float zoom){
        if (mGmap != null) {

            if(latLng != null && zoom != 0){
                mGmap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
            } else {
                if (mLocationViewModel != null){
                    Location location = mLocationViewModel.getLiveData().getValue();
                    if (location != null){

                        mGmap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                new LatLng(location.getLatitude(), location.getLongitude()),
                                LocationConstants.ZOOM_LEVEL_USER_LOCATION));
                    }
                }
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGmap = googleMap;
        mSchoolClusterPresenter = new SchoolClusterPresenter(getActivity(), mGmap);

        mGmap.setOnCameraMoveStartedListener(reason -> {
            // if user moved the camera then don't automatically move it to location
            if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
                shallMoveCameraToLocation = false;
            }
        });

        mGmap.setOnInfoWindowClickListener(marker -> {
            FragmentManager fm = getActivity().getSupportFragmentManager();

            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.fragment_layout,
                    SchoolDetailFragment.newInstance(marker.getTitle()));
            ft.addToBackStack(null);
            ft.commit();
        });

        if(didWeMissPinLoading){
            didWeMissPinLoading = false;
            loadPinsOnMap();
        }

        if(!shallMoveCameraToLocation) {
            moveMapCamera(LocationConstants.NYLATLNG, LocationConstants.ZOOM_LEVEL_CITY);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putBoolean(SI_SHALL_MOVE_CAMERA_BASED_ON_LOCATION, shallMoveCameraToLocation);
        super.onSaveInstanceState(outState);
    }


    // ----------Boilerplate code ----------------------------------------

    // Ideally I can use MapFragment but I wanted to use the search bar
    // and other fancy filtering logic.

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        mMapView.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mMapView.onStop();
    }
    @Override
    public void onPause() {
        mMapView.onPause();
        super.onPause();
    }
    @Override
    public void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}
