package com.example.hemant.a20181003_hemantgarg_nycschools.ui.jetpack;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.location.Location;

public class LocationViewModel extends ViewModel {

    // LiveData should be created here and then it will be filled with actual data/partial data by Repo.
    private LocationLiveData mLiveData = new LocationLiveData();


    public MutableLiveData<Location> getLiveData(){
        return mLiveData;
    }

    public void triggerLocationUpdate(){
        mLiveData.getLocation();
    }
}
