package com.example.hemant.a20181003_hemantgarg_nycschools.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.hemant.a20181003_hemantgarg_nycschools.R;
import com.example.hemant.a20181003_hemantgarg_nycschools.repo.SchoolRepositoryImp;
import com.example.hemant.a20181003_hemantgarg_nycschools.ui.jetpack.Injection;
import com.example.hemant.a20181003_hemantgarg_nycschools.ui.jetpack.SchoolDetailViewModel;
import com.example.hemant.a20181003_hemantgarg_nycschools.ui.jetpack.SchoolListViewModel;
import com.example.hemant.a20181003_hemantgarg_nycschools.utils.UIUtils;

public class SchoolDetailFragment extends Fragment {

    private static final String ARG_SCHOOL_ID = "ARG_SCHOOL_ID";

    private SchoolDetailViewModel mViewModel;

    public static SchoolDetailFragment newInstance(String schoolId ) {
        SchoolDetailFragment schoolDetailFragment = new SchoolDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_SCHOOL_ID, schoolId);
        schoolDetailFragment.setArguments(bundle);
        return schoolDetailFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_school_detail, container, false);

        Bundle bundle = getArguments();
        String schoolId = "";
        if (bundle != null) {
            schoolId = bundle.getString(ARG_SCHOOL_ID);
        }

        ImageButton imageButton = (ImageButton)view.findViewById(R.id.cancel_action);
        imageButton.setOnClickListener(v -> {
            // kill the fragment
            getFragmentManager().beginTransaction()
                    .remove(SchoolDetailFragment.this).commit();
        });
        final TextView title = view.findViewById(R.id.title);
        final TextView subtitle = view.findViewById(R.id.subtitle);

        final TextView finalGradesValue = view.findViewById(R.id.value_3);
        final TextView attendanceRateValue = view.findViewById(R.id.value_2);
        final TextView successRateValue = view.findViewById(R.id.value_1);

        final TextView finalGradesTitle = view.findViewById(R.id.title_3);
        final TextView attendanceRateTitle = view.findViewById(R.id.title_2);
        final TextView successRateTitle = view.findViewById(R.id.title_1);

        final TextView phoneNumberView = view.findViewById(R.id.phone_number);
        final TextView emailView = view.findViewById(R.id.email);
        final TextView websiteView = view.findViewById(R.id.website);

        final TextView overviewView = view.findViewById(R.id.other_mesg_value);

        final LinearLayout satScoreLayout = view.findViewById(R.id.sat_score_layout);
        final TextView studentCount = satScoreLayout.findViewById(R.id.num_of_students);
        final TextView writingScoreValue = satScoreLayout.findViewById(R.id.value_3);
        final TextView mathScoreValue = satScoreLayout.findViewById(R.id.value_2);
        final TextView readingScoreValue = satScoreLayout.findViewById(R.id.value_1);

        final TextView writingScoreTitle = satScoreLayout.findViewById(R.id.title_3);
        final TextView mathScoreTitle = satScoreLayout.findViewById(R.id.title_2);
        final TextView readingScoreTitle = satScoreLayout.findViewById(R.id.title_1);



        mViewModel = ViewModelProviders
                .of(this.getActivity(), Injection.provideViewModelFactory(this.getActivity()))
                .get(SchoolDetailViewModel.class);

        mViewModel.getLiveData().observe(this, schoolsResponse -> {

            title.setText(schoolsResponse.school.name);
            subtitle.setText(schoolsResponse.school.academicOpportunities1);

            finalGradesTitle.setText(R.string.label_final_grades);
            if(!TextUtils.isEmpty(schoolsResponse.school.finalGrades)) {
                finalGradesValue.setText(schoolsResponse.school.finalGrades);
            }

            attendanceRateTitle.setText(R.string.label_attendance_rate);
            if(!TextUtils.isEmpty(schoolsResponse.school.attendanceRate)) {
                attendanceRateValue.setText(UIUtils.getPercentage(schoolsResponse.school.attendanceRate));
            }

            successRateTitle.setText(R.string.label_carrer_success_rate);
            if(!TextUtils.isEmpty(schoolsResponse.school.careerRate)) {
                successRateValue.setText(UIUtils.getPercentage(schoolsResponse.school.careerRate));
            }

            if (schoolsResponse.satScore != null){

                if(!TextUtils.isEmpty(schoolsResponse.satScore.testTakers)) {
                    studentCount.setText(getString(R.string.sat_student_count, schoolsResponse.satScore.testTakers));
                }

                mathScoreTitle.setText(R.string.math_score_title);
                if(!TextUtils.isEmpty(schoolsResponse.satScore.avgMathScore)) {
                    mathScoreValue.setText(schoolsResponse.satScore.avgMathScore);
                }


                writingScoreTitle.setText(R.string.writing_score_title);
                if(!TextUtils.isEmpty(schoolsResponse.satScore.avgWritingScore)) {
                    writingScoreValue.setText(schoolsResponse.satScore.avgWritingScore);
                }

                readingScoreTitle.setText(R.string.reading_score_title);
                if(!TextUtils.isEmpty(schoolsResponse.satScore.avgReadingScore)) {
                    readingScoreValue.setText(schoolsResponse.satScore.avgReadingScore);
                }
            }

            if(!TextUtils.isEmpty(schoolsResponse.school.phone)) {
                phoneNumberView.setText(getString(R.string.phone_number, schoolsResponse.school.phone));
            }

            if(!TextUtils.isEmpty(schoolsResponse.school.email)) {
                emailView.setText(getString(R.string.email, schoolsResponse.school.email));
            }

            if(!TextUtils.isEmpty(schoolsResponse.school.website)) {
                websiteView.setText(getString(R.string.website, schoolsResponse.school.website));
            }

            if(!TextUtils.isEmpty(schoolsResponse.school.overview)) {
                overviewView.setText(schoolsResponse.school.overview);
            }
        });


        if(savedInstanceState == null){
            mViewModel.getSchoolResponse(schoolId);
        }
        return view;
    }

}
