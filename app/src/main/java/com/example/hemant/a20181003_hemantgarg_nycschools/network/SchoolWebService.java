package com.example.hemant.a20181003_hemantgarg_nycschools.network;

import com.example.hemant.a20181003_hemantgarg_nycschools.models.SATScore;
import com.example.hemant.a20181003_hemantgarg_nycschools.models.School;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SchoolWebService {
    @GET("resource/97mf-9njv.json")
    Call<List<School>> getSchool(@Query("$q") String searchText);

    @GET("resource/97mf-9njv.json")
    Call<List<School>> getAllSchools();


    @GET("resource/734v-jeq5.json")
    Call<List<SATScore>> getSATScore(@Query("dbn") String schoolId);
}
