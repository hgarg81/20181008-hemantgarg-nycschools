package com.example.hemant.a20181003_hemantgarg_nycschools.ui.jetpack;

import android.arch.lifecycle.MutableLiveData;

import com.example.hemant.a20181003_hemantgarg_nycschools.models.SchoolFilter;
import com.example.hemant.a20181003_hemantgarg_nycschools.models.SchoolsResponse;
import com.example.hemant.a20181003_hemantgarg_nycschools.repo.SchoolRepository;

public class SchoolListViewModel extends SchoolViewModel<SchoolsResponse> {


    public SchoolListViewModel(SchoolRepository schoolRepository) {
        super(schoolRepository);
    }

    public MutableLiveData<SchoolsResponse> getFilteredSchoolList(SchoolFilter schoolFilter){
        getSchoolRepository().getSchoolList(mLiveData, schoolFilter);
        return mLiveData;
    }

}
