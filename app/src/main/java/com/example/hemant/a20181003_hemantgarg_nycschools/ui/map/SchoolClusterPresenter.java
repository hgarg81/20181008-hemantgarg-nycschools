package com.example.hemant.a20181003_hemantgarg_nycschools.ui.map;

import android.content.Context;
import android.text.TextUtils;

import com.example.hemant.a20181003_hemantgarg_nycschools.App;
import com.example.hemant.a20181003_hemantgarg_nycschools.models.School;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterManager;

import java.util.List;

/**
 * This class is helper to main activity to separate out the Cluster logic.
 * This can be easily generalized to be shared across different fragment/activities
 * needing the same logic.
 * For the sake of time I left is hardcoded.
 */
public class SchoolClusterPresenter {

    private ClusterManager<SchoolMapClusterItem> mClusterManager;
    private GoogleMap mGmap;
    private SchoolInfoWindowAdapter mSchoolInfoWindowAdapter;

    public SchoolClusterPresenter(Context ctx, GoogleMap gMap){
        mGmap = gMap;
        mSchoolInfoWindowAdapter = new SchoolInfoWindowAdapter(ctx);

        mGmap.setInfoWindowAdapter(mSchoolInfoWindowAdapter);
        mGmap.setMinZoomPreference(LocationConstants.ZOOM_LEVEL_CITY);

        UiSettings uiSettings = mGmap.getUiSettings();
        uiSettings.setMyLocationButtonEnabled(true);
        uiSettings.setMapToolbarEnabled(true);
        uiSettings.setCompassEnabled(true);
        uiSettings.setZoomControlsEnabled(true);

        mClusterManager = new ClusterManager<>(App.getAppContext(), mGmap);
        mClusterManager.setRenderer(new FixedMapClusterRenderer<>(App.getAppContext(),
                mGmap, mClusterManager));

        mClusterManager.setAnimation(true);
        mGmap.setOnCameraIdleListener(mClusterManager);
        mGmap.setOnMarkerClickListener(mClusterManager);

        // I want to zoom in when a cluster is clicked.
        // default cluster renderer is not doing that.
        mClusterManager.setOnClusterClickListener(cluster -> {
            mGmap.moveCamera(CameraUpdateFactory.zoomIn());
            return false;
        });
    }

    public void clearAll(){
        mGmap.clear();
        mClusterManager.clearItems();
    }

    /**
     *
     * @param schools
     * @return - true if there were clusters found with lat lng
     */
    public boolean loadClusters(List<School> schools){

        mSchoolInfoWindowAdapter.setSchoolData(schools);

        // this removes all markers before adding new.
        clearAll();
        boolean result = false;

        LatLng firstLatLng = null;

        for(School school: schools){
            LatLng latLng = null;

            if(!TextUtils.isEmpty(school.latitude) && !TextUtils.isEmpty(school.longitude)) {
                try {
                    latLng = new LatLng(Double.parseDouble(school.latitude),
                            Double.parseDouble(school.longitude));
                }catch (NumberFormatException ex){
                    //TODO Report this to Crashlytics; someone messed the data.
                }
            }

            if(latLng == null){
                continue;
            }

            if (firstLatLng == null) {
                firstLatLng = latLng;
            }

            mClusterManager.addItem(new SchoolMapClusterItem(school));
        }

        if(firstLatLng != null) {
            result = true;
            mGmap.moveCamera(CameraUpdateFactory.newLatLngZoom(firstLatLng, LocationConstants.ZOOM_LEVEL_CITY));
        }
        mClusterManager.cluster();
        return result;
    }
}
