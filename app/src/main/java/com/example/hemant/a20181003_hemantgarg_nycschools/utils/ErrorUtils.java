package com.example.hemant.a20181003_hemantgarg_nycschools.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.hemant.a20181003_hemantgarg_nycschools.App;
import com.example.hemant.a20181003_hemantgarg_nycschools.R;
import com.example.hemant.a20181003_hemantgarg_nycschools.models.Status;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Response;

public class ErrorUtils {

    public static final String NETWORK_ERROR_SERVER_DOWN = "NETWORK_ERROR_SERVER_DOWN";
    public static final String NETWORK_ERROR_DEVICE_HAS_NO_NETWORK = "NETWORK_ERROR_DEVICE_HAS_NO_NETWORK";

    public static Status handleWebserviceError(Context context, Throwable throwable){
        Status status = new Status();

        if (!isDeviceConnectedToInternet(context)){
            status.code = NETWORK_ERROR_DEVICE_HAS_NO_NETWORK;
            status.message = context.getString(R.string.error_network_no_internet);
        } else if (throwable instanceof IOException){
            // this could happen because our server is down.
            status.code = NETWORK_ERROR_SERVER_DOWN;
            status.message = context.getString(R.string.error_network_server_down);
        }
        return status;

    }

    public static Status handleWebserviceError(Context context, Response<?> response){
        Status status;

        if(response.errorBody() != null){
            try {
                status = ErrorUtils.fromServerError(context, response.errorBody().string());
            } catch (IOException e) {
                status = ErrorUtils.fromNetworkError(context, response.code(), e.getMessage());
            }
        } else{
            status = ErrorUtils.fromNetworkError(context, response.code(), response.message());
        }
        return status;
    }

    private static Status fromNetworkError(Context context, int code, String message){
        Status status = new Status();
        status.code = String.valueOf(code);
        status.message = message;
        status.isNetworkError = true;
        return status;
    }

    public static Status fromServerError(Context context, String serverError){
        Gson gson = new Gson();
        Status status = gson.fromJson(serverError, Status.class);
        return status;
    }

    private static boolean isDeviceConnectedToInternet(Context context){

        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnected();
    }
}
