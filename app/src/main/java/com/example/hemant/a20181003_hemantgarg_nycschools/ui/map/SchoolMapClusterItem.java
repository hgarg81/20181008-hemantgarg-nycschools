package com.example.hemant.a20181003_hemantgarg_nycschools.ui.map;

import com.example.hemant.a20181003_hemantgarg_nycschools.models.School;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Class required for clustering
 */
public class SchoolMapClusterItem implements ClusterItem {

    private School mSchool;
    private LatLng mPosition;

    public SchoolMapClusterItem(School school){
        this.mSchool = school;
        mPosition = new LatLng(Double.parseDouble(school.latitude),
                Double.parseDouble(school.longitude));

    }
    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public String getTitle() {
        return mSchool.id;
    }

    @Override
    public String getSnippet() {
        return mSchool.borough;
    }
}
