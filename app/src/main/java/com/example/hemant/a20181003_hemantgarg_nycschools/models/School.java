package com.example.hemant.a20181003_hemantgarg_nycschools.models;


import com.google.gson.annotations.SerializedName;

/**
 * Simple model class
 */
public class School {

    @SerializedName("dbn")
    public String id;

    public String latitude;
    public String longitude;

    @SerializedName("school_name")
    public String name;

    @SerializedName("school_email")
    public String email;


    public String borough;

    public String website;

    @SerializedName("total_students")
    public String totalStudents;

    @SerializedName("academicopportunities1")
    public String academicOpportunities1;

    @SerializedName("college_career_rate")
    public String careerRate;

    @SerializedName("end_time")
    public String endTime;

    @SerializedName("start_time")
    public String startTime;

    @SerializedName("finalgrades")
    public String finalGrades;

    @SerializedName("attendance_rate")
    public String attendanceRate;

    @SerializedName("overview_paragraph")
    public String overview;

    @SerializedName("phone_number")
    public String phone;

    @SerializedName("primary_address_line_1")
    public String address;


    public String getId() {
        return id;
    }
}
