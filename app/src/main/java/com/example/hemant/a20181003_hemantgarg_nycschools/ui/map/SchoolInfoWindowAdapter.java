package com.example.hemant.a20181003_hemantgarg_nycschools.ui.map;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.example.hemant.a20181003_hemantgarg_nycschools.R;
import com.example.hemant.a20181003_hemantgarg_nycschools.models.School;
import com.example.hemant.a20181003_hemantgarg_nycschools.utils.UIUtils;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This call display the info on top of each map pin.
 * We wanted to customize that UI.
 * There is a trick here to use school id in the title
 */
public class SchoolInfoWindowAdapter implements InfoWindowAdapter {

    private Context mContext;
    private Map<String, School> mSchoolMap = new HashMap<>();

    public SchoolInfoWindowAdapter(Context ctx){
        mContext = ctx;
    }

    public School getSchool(String schoolId){
        return mSchoolMap.get(schoolId);
    }

    public void setSchoolData(List<School> schoolList){
        mSchoolMap = schoolList.stream().collect(Collectors.toMap(School::getId, school -> school));
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {

        // this is a hack to not display the custom UI on clusters
        if(TextUtils.isEmpty(marker.getTitle())){
            return null;
        }

        School school = getSchool(marker.getTitle());
        if(school == null){
            return null;
        }

        View view = ((Activity)mContext).getLayoutInflater()
                .inflate(R.layout.school_list_item, null);

        TextView title = (TextView) view.findViewById(R.id.title);
        TextView subtitle = (TextView) view.findViewById(R.id.subtitle);

        TextView finalGradesValue = (TextView) view.findViewById(R.id.value_3);
        TextView attendanceRateValue = (TextView) view.findViewById(R.id.value_2);
        TextView successRateValue = (TextView) view.findViewById(R.id.value_1);

        TextView finalGradesTitle = (TextView) view.findViewById(R.id.title_3);
        TextView attendanceRateTitle = (TextView) view.findViewById(R.id.title_2);
        TextView successRateTitle = (TextView) view.findViewById(R.id.title_1);

        title.setText(school.name);
        subtitle.setText(school.academicOpportunities1);

        finalGradesTitle.setText(R.string.label_final_grades);
        if(!TextUtils.isEmpty(school.finalGrades)) {
            finalGradesValue.setText(school.finalGrades);
        }

        attendanceRateTitle.setText(R.string.label_attendance_rate);
        if(!TextUtils.isEmpty(school.attendanceRate)) {
            attendanceRateValue.setText(UIUtils.getPercentage(school.attendanceRate));
        }

        successRateTitle.setText(R.string.label_carrer_success_rate);
        if(!TextUtils.isEmpty(school.careerRate)) {
            successRateValue.setText(UIUtils.getPercentage(school.careerRate));
        }
        return view;
    }
}
