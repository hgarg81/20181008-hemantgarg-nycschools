package com.example.hemant.a20181003_hemantgarg_nycschools.models;

import com.google.gson.Gson;

public class Status {
    /**
     * This could be errorcode from your server,
     * HTTP error code or just a status message that data is being fetched
     */
    public String code;


    public String message;
    public boolean isNetworkError;

}
