package com.example.hemant.a20181003_hemantgarg_nycschools.ui.map;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

// Android is hard - Google's demo sample has a bug.
// I have to override this to overcome cluster click bug.
// somehow if the cluster size is less than 6 then it doesn't go away.
// The hack is to increase the cluster size. I randomly choose 10 and it works...:)
public class FixedMapClusterRenderer<T extends ClusterItem> extends DefaultClusterRenderer<T> {

    public FixedMapClusterRenderer(Context context, GoogleMap map,
                             ClusterManager<T> clusterManager) {
        super(context, map, clusterManager);
    }

    @Override
    protected void onBeforeClusterItemRendered(T item, MarkerOptions markerOptions) {
        super.onBeforeClusterItemRendered(item, markerOptions);
        markerOptions.title(item.getTitle());
    }

    @Override
    protected void onClusterItemRendered(T clusterItem, Marker marker) {
        super.onClusterItemRendered(clusterItem, marker);

        //here you have access to the marker itself
    }

    @Override
    protected boolean shouldRenderAsCluster(Cluster<T> cluster) {
        return cluster.getSize() > 10;
    }
}
