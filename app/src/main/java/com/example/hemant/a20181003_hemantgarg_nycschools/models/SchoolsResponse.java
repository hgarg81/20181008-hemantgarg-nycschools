package com.example.hemant.a20181003_hemantgarg_nycschools.models;

import java.util.ArrayList;
import java.util.List;

/**
 * This class powers the
 * We want to keep status and actual result in same object
 * so that UI can decide what they want to show
 */
public class SchoolsResponse {
    public List<School> schools = new ArrayList<>();
    public SchoolFilter filter;
    public Status status;
}
