package com.example.hemant.a20181003_hemantgarg_nycschools.ui.map;

import com.google.android.gms.maps.model.LatLng;

public class LocationConstants {

    public static final int ZOOM_LEVEL_CITY = 10;
    public static final int ZOOM_LEVEL_USER_LOCATION = 13;
    public static LatLng NYLATLNG = new LatLng(40.7143528, -74.0059731); //NYC

}
