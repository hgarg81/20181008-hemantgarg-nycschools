package com.example.hemant.a20181003_hemantgarg_nycschools.ui.jetpack;

import android.arch.lifecycle.LiveData;

import com.example.hemant.a20181003_hemantgarg_nycschools.models.SchoolWithSATResponse;
import com.example.hemant.a20181003_hemantgarg_nycschools.repo.SchoolRepository;

/**
 * It gets the data handed over to UI
 */
public class SchoolDetailViewModel extends SchoolViewModel<SchoolWithSATResponse> {


    public SchoolDetailViewModel(SchoolRepository schoolRepository) {
        super(schoolRepository);
    }

    public LiveData<SchoolWithSATResponse> getSchoolResponse(String schoolId) throws NullPointerException{
       getSchoolRepository().getSchoolWithSATById(mLiveData, schoolId);
       return mLiveData;
    }


}
