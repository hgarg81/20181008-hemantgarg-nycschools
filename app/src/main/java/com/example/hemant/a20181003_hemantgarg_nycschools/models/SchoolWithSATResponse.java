package com.example.hemant.a20181003_hemantgarg_nycschools.models;

import java.util.ArrayList;
import java.util.List;

public class SchoolWithSATResponse {
    public School school;
    public SATScore satScore;
    public Status staScoreStatus;
}
