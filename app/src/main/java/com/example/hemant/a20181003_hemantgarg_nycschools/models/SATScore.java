package com.example.hemant.a20181003_hemantgarg_nycschools.models;


import com.google.gson.annotations.SerializedName;

/**
 * Simple model class
 */
public class SATScore {

    @SerializedName("dbn")
    public String id;

    @SerializedName("num_of_sat_test_takers")
    public String testTakers;

    @SerializedName("sat_critical_reading_avg_score")
    public String avgReadingScore;

    @SerializedName("sat_math_avg_score")
    public String avgMathScore;

    @SerializedName("sat_writing_avg_score")
    public String avgWritingScore;

}
