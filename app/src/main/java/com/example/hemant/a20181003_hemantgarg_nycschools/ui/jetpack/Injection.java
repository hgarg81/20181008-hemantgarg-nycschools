package com.example.hemant.a20181003_hemantgarg_nycschools.ui.jetpack;

import android.content.Context;

import com.example.hemant.a20181003_hemantgarg_nycschools.network.SchoolWebService;
import com.example.hemant.a20181003_hemantgarg_nycschools.repo.SchoolRepository;
import com.example.hemant.a20181003_hemantgarg_nycschools.repo.SchoolRepositoryImp;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Injection {

    public static SchoolRepository provideSchoolRepository(Context context) {
        SchoolWebService schoolWebService = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://data.cityofnewyork.us/")
                .build().create(SchoolWebService.class);

        return SchoolRepositoryImp.getInstance(context, schoolWebService);
    }

    public static ViewModelFactory provideViewModelFactory(Context context) {
        return new ViewModelFactory(provideSchoolRepository(context));
    }
}
