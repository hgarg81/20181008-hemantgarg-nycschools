package com.example.hemant.a20181003_hemantgarg_nycschools.ui.jetpack;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.hemant.a20181003_hemantgarg_nycschools.repo.SchoolRepository;

public abstract class SchoolViewModel<t> extends ViewModel {

    private SchoolRepository mSchoolRepository;

    protected MutableLiveData<t> mLiveData = new MutableLiveData<>();

    public SchoolViewModel(SchoolRepository schoolRepository){
        mSchoolRepository = schoolRepository;
    }

    public MutableLiveData<t> getLiveData(){
        return mLiveData;
    }

    protected SchoolRepository getSchoolRepository(){
        return mSchoolRepository;
    }
}
